<?php
require_once dirname(__FILE__) . '/less.inc.php';
namespace tarrel\less;

/**
 * @author: Tarrel
 * @email: juantarrel@gmail.com
 * Date: 25.03.2015
 */
class AutoloadLess extends \yii\base\Widget
{

    private static $_sCachePath = "";
    private static $_sCssPath   = "";
    private static $_aWatchList = array();

    /**
     * Destino para los archivos cachados
     */
    public static function cachePath($sPath) {
        return (is_string($sPath) && (($sPath = trim($sPath)) != "")) ? self::$_sCachePath = ($sPath . "/") : self::$_sCachePath;
    }

    /**
     * Destino para los archivos css
     */
    public static function cssPath($sPath) {
        return (is_string($sPath) && (($sPath = trim($sPath)) != "")) ? self::$_sCssPath = ($sPath . "/") : self::$_sCssPath;
    }

    public static function addFileToWatchList($sFilename) {
        if (is_array($sFilename)) {
            foreach ($sFilename as $sFile) {
                if (is_string($sFile) && (($sFile = trim($sFile)) != "")) self::$_aWatchList[] = $sFile;
            }
        } elseif (is_string($sFilename) && (($sFilename = trim($sFilename)) != "")) {
            self::$_aWatchList[] = $sFilename;
        }
    }

    /**
     * Genera el objeto less con la configuracion basica
     *
     * @return lessc
     */
    private static function _getLessObject() {
        $oLess = new lessc;
        $oLess->setPreserveComments(false);

        //if (Aqui una bandera para saber si esta en dev y genere el formateado con less o comprimido) {
        if(!YII_DEBUG){
            $oLess->setFormatter("lessjs");
        }else {
            $oLess->setFormatter("compressed");
        }

        return $oLess;
    }

    /**
     * Se llama para ver cada arvhico en el watch list
     * Funciona solo si la variable LESS_WATCH esta en true
     * @return bool
     */
    public static function check() {
        $bReturn = true;
        if ((self::$_sCssPath != "") && (self::$_sCachePath != "")) {
            // Aqui se puede crear una bandera para ver si esta en dev o produccion, si esta en dev entonces entra{
            if(YII_DEBUG){
                foreach (self::$_aWatchList as $sFile) {
                    $bReturn = self::_cachedCompile($sFile, self::$_sCssPath . pathinfo($sFile, PATHINFO_FILENAME) . ".css") && $bReturn;
                }
            }
        }

        return $bReturn;
    }

    /**
     * Compila un archivo
     *
     * @return string
     */
    public static function compile($sInputFile, $sOutputFile = null) {
        $sReturn = '';

        try {
            $oLess   = self::_getLessObject();
            $sReturn = $oLess->compileFile($sInputFile, $sOutputFile);
        } catch (exception $e) {
            error_log("LessHandler::compile(): " . $e->getMessage());
        }

        return $sReturn;
    }

    /**
     * Compila un string
     *
     * @return string
     */
    public static function stringCompile($sInputString, $sOutputFile = null) {
        $sReturn = '';

        try {
            $oLess   = self::_getLessObject();
            $sReturn = $oLess->compile($sInputFile, $sOutputFile);
            if ($sOutputFile !== null) {
                file_put_contents($sOutputFile, $sReturn);
            }
        } catch (exception $e) {
            error_log("LessHandler::stringCompile(): " . $e->getMessage());
        }

        return $sReturn;
    }

    /**
     * Ve su ek archivo de entrada es nuevo luego te lo regresa, si el archivo no existe entonces lo compila
     *
     * @return bool
     */
    private static function checkedCompile($sInputFile, $sOutputFile) {
        $bReturn = false;

        try {
            $oLess   = self::_getLessObject();
            $bReturn = $oLess->checkedCompile($sInputFile, $sOutputFile);
        } catch (exception $e) {
            error_log("LessHandler::checkedCompile(): " . $e->getMessage());
        }

        return $bReturn;
    }

    /**
     * Ve su ek archivo de entrada es nuevo luego te lo regresa, si el archivo no existe entonces lo compila
     * Mira todos los cambio hasta los importados tambien ;)
     */
    private static function _cachedCompile($sInputFile, $sOutputFile) {
        // Carga el cache
        $sCacheFile = self::$_sCachePath . basename( $sInputFile . ".cache" );

        if (file_exists($sCacheFile)) {
            $aCache = unserialize(file_get_contents($sCacheFile));
        } else {
            $aCache = $sInputFile;
        }

        try {
            $oLess     = self::_getLessObject();
            $aNewCache = $oLess->cachedCompile($aCache);
            if (!is_array($aCache) || $aNewCache["updated"] > $aCache["updated"]) {
                file_put_contents($sCacheFile, serialize($aNewCache));
                file_put_contents($sOutputFile, $aNewCache['compiled']);
            }
        } catch (exception $e) {
            error_log("LessHandler::_cachedCompile(): " . $e->getMessage());
        }
    }    
}
