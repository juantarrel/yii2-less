Less compiler
=============
This is a less compiler

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist tarrel/yii2-less "*"
```

or add

```
"tarrel/yii2-less": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \tarrel\less\AutoloadLess::widget(); ?>```